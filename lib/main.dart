import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const LoginPageUI(),
    );
  }

}

class LoginPageUI extends StatelessWidget {
  const LoginPageUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.only(top: 20),
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Image.asset('images/cloudgate_logo.jpg', scale: 2),
                      const Center(
                        child: Text(
                          "Sign In",
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: 25
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Expanded(
                              child: TextField(
                                onChanged: (email) {
                                  // loginProvider.userEmail = email;
                                },
                                textAlign: TextAlign.center,
                                decoration: const InputDecoration(
                                    icon: Icon(Icons.email),
                                    hintText: "Enter your email address",
                                    border: InputBorder.none
                                ),
                              )
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: const Text(
                          "or",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Expanded(
                              child: TextField(
                                onChanged: (phone){
                                  // loginProvider.userPhoneNumber = phone;
                                },
                                textAlign: TextAlign.center,
                                decoration: const InputDecoration(
                                    icon: Icon(Icons.phone_android_sharp),
                                    hintText: "Enter your mobile number",
                                    border: InputBorder.none
                                ),
                              )
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      const SizedBox(height: 10),
                      MaterialButton(
                        height: 40,
                        color: const Color(0xff65ce34),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: const Text(
                          "Next",
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                        onPressed: (){
                          // Get.to(() => OSCorpDesign());
                          // loginProvider.onNextTapped(context);
                        },
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: const Text(
                          "Or scan QR on the profile page of web portal",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.all(2),
                    color: Colors.blue,
                    child: const Icon(
                      Icons.qr_code_sharp,
                      size: 40,
                      color: Colors.white,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: const Text(
                      "Scan QR Code",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.blue,
                          fontSize: 14
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

